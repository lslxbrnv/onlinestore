<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$this->get('/', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login')->name('login_auth');
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register')->name('register_auth');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

$this->get('forgot-password', 'Auth\RegisterController@forgot_password_form')->name('forgot_password_form');
$this->post('verify-user-forgot-pass', 'Auth\RegisterController@verify_user_forgot_password')->name('verify_user_forgot_password');


Route::prefix('portal')
->middleware('auth')
->group(function(){
	$this->post('logout', 'Auth\LoginController@logout')->name('logout');
	Route::get('reset-password', 'UsersController@reset_password_form')->name('reset_password_form');
	Route::post('reset-password', 'UsersController@reset_password')->name('reset_password');

	#receipt
	Route::get('/official-receipt/{id?}', 'TransactionController@official_receipt')->name('official_receipt');

	Route::prefix('cust')
	->group(function(){
		Route::get('/', 'HomeController@cust')->name('cust');
		Route::get('/items/{id?}', 'CustomerController@index')->name('cust_items');
		Route::get('/transactions', 'CustomerController@transactions')->name('cust_transactions');
		#Route::get('/get-item', 'CustomerController@get_item')->name('user_get_item');

		Route::post('/add-to-cart', 'CustomerController@add_to_cart')->name('add_to_cart');

		Route::get('/confirmed-customer-details', 'CustomerController@confirm_customer_details')->name('confirm_customer_details');

		Route::get('/proceed-to-checkout', 'CustomerController@proceed_to_checkout')->name('proceed_to_checkout');
		Route::get('/remove-item-from-cart/{id?}', 'CustomerController@remove_item_from_cart')->name('remove_item_from_cart');
		
		Route::get('/confirmed-customer-details', 'CustomerController@confirm_customer_details')->name('confirm_customer_details');
		Route::post('/confirmed-checkout', 'CustomerController@confirmed_checkout')->name('confirmed_checkout');
		
		#success_order
		Route::get('/order-success/{id?}', 'CustomerController@order_success')->name('order_success');
		Route::get('/view-transaction/{id?}', 'CustomerController@view_transaction')->name('view_cust_transaction');

	});

	Route::prefix('admin')
	->middleware('AdminAccess')
	->group(function(){
		Route::get('/', 'HomeController@index')->name('home');
		Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
		Route::get('/categories', 'CategoryController@index')->name('categories');
		Route::get('/add-category', 'CategoryController@add_category')->name('add_category');
		Route::post('/submit-category', 'CategoryController@submit_category')->name('submit_category');
		Route::get('/edit-category/{id?}', 'CategoryController@edit_category')->name('edit_category');
		Route::get('/delete-category', 'CategoryController@delete_category')->name('delete_category');

		Route::get('/items', 'ItemController@index')->name('items');
		Route::post('/submit-item', 'ItemController@submit_item')->name('submit_item');
		Route::get('/add-item', 'ItemController@add_item')->name('add_item');
		Route::get('/edit-item/{id?}', 'ItemController@edit_item')->name('edit_item');
		Route::get('/delete-item', 'ItemController@delete_item')->name('delete_item');

		Route::get('/transactions','TransactionController@index')->name('transactions');
		Route::get('/view-transaction/{id?}','TransactionController@view_transaction')->name('view_transaction');

		Route::get('/accept-order/{id?}', 'TransactionController@accept_order')->name('accept_order');
		Route::get('/invalid-order/{id?}', 'TransactionController@invalid_order')->name('invalid_order');

		Route::resource('users', 'UsersController');
		Route::get('/disable-user', 'UsersController@disable_user')->name('disable_user');
		Route::get('generate-password', 'UsersController@generate_password')->name('generate_password');
	});
});
