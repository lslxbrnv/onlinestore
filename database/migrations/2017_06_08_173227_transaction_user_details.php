<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionUserDetails extends Migration
{
    public function up()
    {
        Schema::create('transaction_user_details', function(Blueprint $table) 
        {
            $table->increments('id');

            $table->string('address');
            $table->string('contact_number');
            
            $table->string('location')->nullable();

            $table->integer('transaction_id')->unsigned();

            $table->timestamps();
            
            $table->foreign('transaction_id')->references('id')->on('transaction');
        });
    }

    public function down()
    {
        Schema::drop('transaction_user_details');
    }
}
