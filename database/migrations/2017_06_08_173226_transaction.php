<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transaction extends Migration
{
    public function up()
    {
        Schema::create('transaction', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->decimal('cash_received', 7,2);
            $table->integer('user_id')->unsigned();
            $table->dateTime('delivery_date');
            $table->tinyInteger('status');

            $table->timestamps();
            
            $table->index(['user_id']);

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::drop('transaction');
    }
}
