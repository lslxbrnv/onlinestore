<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionItems extends Migration
{
    public function up()
    {
        Schema::create('transaction_items', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->decimal('price', 5, 2);
            $table->integer('quantity');
            
            $table->index(['transaction_id']);

            $table->foreign('transaction_id')->references('id')->on('transaction');
            $table->foreign('item_id')->references('id')->on('item');
        });
    }

    public function down()
    {
        Schema::drop('transaction_items');
    }
}
