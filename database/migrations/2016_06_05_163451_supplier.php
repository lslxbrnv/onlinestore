<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Supplier extends Migration
{
    public function up()
    {
        Schema::create('supplier', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->integer('contact_no');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('supplier');
    }
}
