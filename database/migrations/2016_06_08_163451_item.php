<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Item extends Migration
{
    public function up()
    {
        //
        Schema::create('item', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name');
            $table->string('details');
            $table->integer('category_id')->unsigned();
            $table->integer('supplier_id')->unsigned();
            $table->decimal('price',5,2);
            $table->boolean('available')->default(0); 
            $table->string('file')->default(null);   
            $table->timestamps();
            $table->softDeletes(); 

            $table->index(['supplier_id', 'category_id']);

            $table->foreign('category_id')->references('id')->on('category');
            $table->foreign('supplier_id')->references('id')->on('supplier');
        });
    }

    public function down()
    {
        //
        Schema::drop('item');
    }
}
