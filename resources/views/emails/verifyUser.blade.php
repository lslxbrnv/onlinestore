<!DOCTYPE html>
<html>
<head>
    <title>KMart Online Grocery Registration Confirmation</title>
</head>
 
<body>
<h2>Welcome to Kmart Online Grocery {{$user['name']}}</h2>
<br/>
Your registered email is {{$user['email']}} , 

Please click on the link below to verify your email account
<br/>
<a href="{{url('user/verify', $user->verifyUser->token)}}">Verify Email</a>
</body>
 
</html>