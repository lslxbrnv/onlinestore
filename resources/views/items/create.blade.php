@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="{{ route('items') }}">Items</a></li>
                    <li class="is-active"><a href="#" aria-current="page">{{ $pagetype }}</a></li>
                </ul>
            </nav>
            </header>
            <div class="card-content">
                @include('layouts.validation-messages')
                <div class="content">
        
                    <form action="{{ route('submit_item') }}" method="post" enctype="multipart/form-data">
                         <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="category_id">Category</label>
                                </div>
                            </div>
                            <div class="column is-10">
                                <div class="field-body is-fullwidth">
                                      <div class="select">
                                        <select name="category_id">
                                            <option value="">-- Select --</option>
                                            @foreach ($categories as $category)
                                                @if(isset($item))
                                                <option value="{{ $category->id }}" @if($category->id=== $item->category_id) selected='selected' @endif>{{ $category->name }}</option>
                                                @else
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="name">Item Name</label>
                                </div>
                            </div>
                            <div class="column is-8">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="name" id="name" value="{{ $item->name or old('name') }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="price">Price</label>
                                </div>
                            </div>
                            <div class="column is-5">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="price" id="price" value="{{ $item->price or old('price') }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="details">Details</label>
                                </div>
                            </div>
                            <div class="column is-5">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <textarea class="textarea" name="details" id="details"placeholder="Details">{{ $item->details or old('details') }}</textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="available">Is Available</label>
                                </div>
                            </div>
                            <div class="column is-10">
                                <div class="field-body is-fullwidth">
                                      <div class="select">
                                        <select name="available">
                                            @if(isset($item))
                                                 @if($item->available == 1)
                                                <option value="1" selected>Yes</option>
                                                <option value="0">No</option>
                                                @else
                                                <option value="1">Yes</option>
                                                <option value="0" selected>No</option>
                                                @endif
                                            @else
                                                <option value="1">Yes</option>
                                                <option value="0" selected>No</option>
                                            
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="file">Image File</label>
                                </div>
                            </div>
                            <div class="card-image is 3" style="width:200px;">
                                @if(isset($item))
                                <figure class="image is-4by3">
                                    <?php $location = asset('storage/public/'.$item->file);  ?>
                                    <img src="<?php echo $location; ?>" />
                                </figure>
                                @else
                                 <figure class="image is-4by3">
                                    <?php $location = asset('images/no_image.svg');  ?>
                                    <img src="<?php echo $location; ?>" />
                                </figure>
                                @endif
                                <span class="label is-normal">Current Photo</span>   
                            </div>
                             <div class="column is-5">
                                <div class="field-body is-fullwidth">
                                    <div class="file">
                                        <label class="file-label">
                                            <input class="file-input" type="file" name="file">
                                            <span class="file-cta">
                                                <span class="file-icon">
                                                    <i class="fa fa-upload"></i>
                                                </span>
                                                <span class="file-label">
                                                    Choose a file…
                                                </span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal"></div>
                            </div>
                            <div class="column is-2">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            {!! csrf_field() !!}
                                            <input type="hidden" value="{{ $item->id or '0' }}" name="item_id">
                                            <input type="submit" class="button is-info is-normal" value="{{ $pagetype }} Item">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="column is-8">&nbsp;</div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection
