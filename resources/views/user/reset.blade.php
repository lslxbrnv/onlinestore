@extends($extends)

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li class="is-active"><a href="#" aria-current="page">Reset Password</a></li>
                </ul>
            </nav>
        </header>
        <div class="card-content">
            @include('layouts.validation-messages')
            <div class="content">
                <form action="{{ $route }}" method="post">

                    <div class="columns">
                        <div class="column is-10">

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="password">Password</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <input class="input is-normal" type="password" name="password" id="password" value="">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="confirm_password">Confirm Password</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <input class="input is-normal" type="password" name="confirm_password" id="confirm_password" value="">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label"></div>
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <button class="button is-info is-normal">Submit</button>
                                            {!! csrf_field() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection