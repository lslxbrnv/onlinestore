<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>KMart Online Store</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jquery.css') }}" rel="stylesheet">
        <link href="{{ asset('js/datetimepicker/jquery.datetimepicker.css') }}" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/datetimepicker/jquery.datetimepicker.js') }}"></script>
    </head>
    <style>
        /* Background customer portal */
          #app{
            background-image: url("{{ asset('images/bg.png')  }}");
            background-repeat: no-repeat;
            min-height: 900px;
            background-position: center; 
        }
    </style>
    <body>
        <nav id="navMenu" class="navbar has-shadow is-primary" role="navigation" aria-label="main navigation">
            <div class="container">
                <div class="navbar-brand">
                    <a href="{{ route('cust') }}" class="navbar-item">KMart Online Store</a>
                    <div id="navbar-burger-id" class="navbar-burger" data-target="navMenu" onclick="toggleBurger()">
                        <span></span><span></span><span></span>
                    </div>
                </div>
            
                <div class="navbar-menu" id="navbar-menu-id">
                    <div class="navbar-start">
                        <a class="navbar-item" href="{{ route('cust_items') }}"><i class="fa fa-shopping-cart"></i>&nbsp;Shop</a>
                        <a class="navbar-item" href="{{ route('cust_transactions') }}"><i class="fa fa-bars"></i>&nbsp;Transactions</a>
                    </div>

                    <div class="navbar-end">
                        <div class="navbar-item has-dropdown is-hoverable">
                            <a class="navbar-link" href="#"><i class="fa fa-user"></i>&nbsp;</a>
                            <div class="navbar-dropdown">
                                <a href="{{ route('reset_password_form') }}" class="navbar-item" title="Reset Password">Change Password</a>
                                <a href="javascript::void(0)" class="navbar-item" onclick="document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div id="app">
            <div class="container">@yield('content')</div>
        </div>
        <br>
        <footer class="footer">
            <div class="container">
                <div class="content has-text-centered">
                    <p>
                        &copy; Online Store, Inc {{ date('Y') }}
                    </p>
                </div>
            </div>
        </footer>
        <script type="text/javascript">
             // Close mobile & tablet menu on item click
        $('.navbar-item').each(function(e) {
            $(this).click(function(){
            if($('#navbar-burger-id').hasClass('is-active')){
                $('#navbar-burger-id').removeClass('is-active');
                $('#navbar-menu-id').removeClass('is-active');
                }
            });
        });

        // Open or Close mobile & tablet menu
        $('#navbar-burger-id').click(function () {
            if($('#navbar-burger-id').hasClass('is-active')){
            $('#navbar-burger-id').removeClass('is-active');
            $('#navbar-menu-id').removeClass('is-active');
            }else {
            $('#navbar-burger-id').addClass('is-active');
            $('#navbar-menu-id').addClass('is-active');
            }
      });
        </script>
    </body>
</html>
