@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-bars"></i>&nbsp;Transactions</h3>
        </div>
        <div class="column is-3">
        
        </div>
    </div>

     <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr> 
                <th width="10%" class="has-text-centered"></th>
                <th width="30%">Transaction No.</th>
                <th width="30%">Date of Transaction</th>
                <th width="20%">Delivery Date</th>
                <th width="10%">Status</th>
            </tr>
        </thead>
        <tbody>
            @forelse($transactions as $transaction)
            <tr>
                 <td class="has-text-centered">
                    <a href="{{ route('view_transaction', $transaction->id) }}" title="View transaction">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </td>
                <td>{{ $transaction->transaction_number }}</td>
                <td>{{ $transaction->created_at }}</td>
                <td>{{ $transaction->delivery_date }}</td>
                <td>{{ $transaction->current_status }}</td>
            </tr>
            @empty
            <tr>
                <td colspan="5" class="has-text-centered">No Transactions yet</td>
            </tr>
            @endforelse
        </tbody>
    </table>

@endsection
