@extends('layouts.master')

@section('content')

    <br />
    <div class="card">
    	<div class="card-content is-small">
    		<div class="content is-small">
    			<h1>Welcome
    			{{ Auth::user()->name }}!
    			</h1>
    		</div>
    	</div>
    </div>
        
@endsection
