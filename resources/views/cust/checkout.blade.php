@extends('layouts.cust')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-check"></i>&nbsp;Checkout</h3>
        </div>
        <div class="column is-3">
          
        </div>
    </div>
    @if(Session::has('success'))
            <div id="success-cart" class="notification is-success">
                {{ Session::get('success') }}
            </div>
        @endif
    <div class="container is-clearfix">  
         <table class="table is-bordered is-hoverable is-fullwidth">
            <thead>
                <tr> 
                    <th width="5%" class="has-text-centered">

                    </th>
                    <th width="20%"></th>
                    <th width="35%">Item</th>
                    <th width="10%" class="has-text-right">Price</th>
                    <th width="5%" >Quantity</th>
                    <th width="10%">Total per Item</th>
                </tr>
            </thead>
            <tbody>
                @php $grandtotal=0; @endphp
                @if($cart_items != null)
                    @foreach ($cart_items as $cart_item)
                    <tr id="item-row-{{ $cart_item->id }}">   
                        <td class="has-text-centered">
                            <a onclick="DeleteItem()" href="{{ route('remove_item_from_cart', $cart_item->id) }}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="has-text-centered">
                            <div style="text-align: center;" class=inline-flex" style="padding:3px;">
                                <div class="card" style="border:solid 1px #c3c3c3;">
                                    <div class="card-image">
                                        <figure class="image is-2by1">
                                            <?php $location = asset('storage/public/'.$cart_item->items->file);  ?>
                                            <img src="<?php echo $location; ?>" />
                                        </figure>
                                    </div>
                                </div>   
                            </div>
                        </td>
                        <td>{{ $cart_item->items->name }}</td>
                        <td class="has-text-right">P {{ $cart_item->price }}</td>
                        <td>{{ $cart_item->quantity }}</td>

                        <td class="has-text-right">P {{ number_format($total =  $cart_item->price * $cart_item->quantity, 2) }}</td>
                        @php
                            $grandtotal = $grandtotal + $total;
                        @endphp
                    </tr>
                    @endforeach
                @else
                <tr>   
                    <td class="has-text-centered" colspan="6">No Items added.</td>
                </tr>
                @endif
                @if ($cart_items != null)
                <!-- Edit for Delivery Fee -->
                @php $delivery_fee = 180;  @endphp 
                <tr>
                    <td colspan="3"></td>
                    <td class="has-text-right" colspan="2">Delivery Fee : </td>
                    <td class="has-text-right" colspan="1">{{ number_format($delivery_fee, 2) }}</td>
                </tr>
                 <tr>
                    <td colspan="3"></td>
                    <td class="has-text-right" colspan="2">Total Bill : </td>
                    <td class="has-text-right" colspan="1"><strong>{{ number_format($grandtotal+$delivery_fee, 2) }}</strong></td>
                </tr>
                @endif
            </tbody>
        </table>
        @if ($cart_items != null)
        <div class="columns">
            <div class="column is-5"></div>
            <div class="column is-2">
                <!-- <button href="{{ route('confirmed_checkout') }}" onclick="return confirm('Are you sure you want to checkout?')" class="button is-info">
                    Confirm Checkout &nbsp;<i class="fa fa-check-circle"></i>
                </button> -->
                <a href="{{ route('confirm_customer_details') }}" class="button is-info">
                    Continue &nbsp;<i class="fa fa-arrow-right"></i>
                </a> 
            </div>
            <div class="column is-5"></div>
        </div>  <br>
        @endif

    </div>


<script type="text/javascript">
     function DeleteItem()
    {
        confirm("Are you sure to delete this?");
    }
</script>
@endsection
