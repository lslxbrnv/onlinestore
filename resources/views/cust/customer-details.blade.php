@extends('layouts.cust')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-truck"></i>&nbsp;Delivery Details</h3>
        </div>
        <div class="column is-3">
          
        </div>
    </div>
    @if(Session::has('success'))
            <div id="success-cart" class="notification is-success">
                {{ Session::get('success') }}
            </div>
        @endif
    <div class="container is-clearfix">  
         <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
           <small><em>Kindly confirm your details on where and when you would want your items to be delivered.</em></small> 
            </header>
         <div class="card-content">
                @include('layouts.validation-messages')
                <div class="content">
                    <form action="{{ route('confirmed_checkout') }}" method="post">
                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="name">Name</label>
                                </div>
                            </div>
                            <div class="column is-1.">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="name" id="name" value="{{ $user->name }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="adddress">Address</label>
                                </div>
                            </div>
                            <div class="column is-1.">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="address" id="address" value="{{ $user->address }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="contact_number">Contact Number</label>
                                </div>
                            </div>
                            <div class="column is-1.">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="contact_number" id="contact_number" value="{{ $user->contact_number }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="cash_received">Cash on Hand</label>
                                </div>
                            </div>
                            <div class="column is-1.">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="cash_received" id="cash_received" value="{{ old('cash_received') }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="delivery_date">Delivery Date Time</label>
                                </div>
                            </div>
                            <div class="column is-1.">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="delivery_date" id="delivery_date" value="{{ old('delivery_date') }}" readonly>
                                            <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}" readonly>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                          <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="total_bill">Amount To Pay</label>
                                </div>
                            </div>
                            <div class="column is-1.">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <strong> &#x20B1;{{ number_format($total_bill,2) }}</strong>
                                            
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal"></div>
                            </div>
                            <div class="column is-2">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            {!! csrf_field() !!}
                                            <input type="submit" onclick="return confirm('Are you sure you want to submit your order?')" class="button is-info" value="Submit Order">

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="column is-8">&nbsp;</div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){ 
            $('#delivery_date').datetimepicker();
        });
    </script>
@endsection
