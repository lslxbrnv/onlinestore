@extends('layouts.cust')
@section('content')
<br>
 <div class="columns">
    <div class="column is-3">
        <h3 class="title is-3"><i class="fa fa-shopping-cart"></i>&nbsp;Items</h3>
    </div>
    <div class="column is-3"> </div>
    <div class="column is-6">
      <form action="{{ route('cust_items') }}" method="get">
            <div class="field has-addons is-pulled-right">
                <div class="control">
                    <a class="button is-static"><i class="fa fa-search"></i></a>
                </div>
                <div class="control">
                    <input class="input is-normal" type="text" name="search" id="search" value="{{ $search }}">
                </div>
                <div class="control">
                    <button class="button is-info">Search</button>
                </div>
                 <div class="control" style="margin-left:5px;">
                    <a href="{{ route('proceed_to_checkout') }}" class="button is-primary">Proceed &nbsp;<i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="columns">
    <div class="column is-2">
        <aside class="menu">
            <p class="menu-label has-text-primary has-text-weight-bold is-size-6">
                Categories
            </p>
            <ul class="menu-list" style="font-size: 17px; ">
                @if(!is_numeric($id))
                <li><a class="is-active" href="{{ route('cust_items') }}">All</a></li>
                @else
                <li><a class="has-text-black" href="{{ route('cust_items') }}">All</a></li>
                @endif
                @foreach($categories as $category)
                    @if($category->id == $id)
                    <li><a class="is-active" href="{{ route('cust_items', $category->id) }}">{{ $category->name }}</a></li>
                    @else
                    <li><a class="has-text-black" href="{{ route('cust_items', $category->id) }}">{{ $category->name }}</a></li>
                    @endif
                @endforeach
            </ul>
        </aside>
    </div>
    <div class="column is-10">
        <div class="container is-clearfix">
            @if(Session::has('message'))
                <div id="success-cart" class="notification is-success">
                    Item has been added to Cart.
                </div>
            @endif
            @foreach($items as $item)
            <!-- width adjust items container size -->
            <div class="is-pulled-left inline-flex" style="width:220px; padding:5px;">
                <div class="card" style="border:solid 1px black;">
                    <div class="card-image">
                        <figure class="image is-4by3">
                        <?php $location = asset('storage/public/'.$item->file);  ?>
                        <img src="<?php echo $location; ?>" />
                        </figure>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <div class="media-content is-clipped" style="height:80px;">
                                <p class="title is-6" >{{ $item->name }}</p>
                                <p class="subtitle is-6">{{ $item->category->name }}</p>
                                <p class="subtitle is-6">P {{ $item->price }}</p>
                            </div>
                        </div>
                        <div class="content" style="overflow-y: scroll; height:30px; max-height:45px;">
                            <em><small>{{ $item->details }}</em></small>
                        </div>
                        <div class="content has-text-centered">
                            <button type="button" class="button is-primary show-cart" onclick="GetMenuId({{ $item->id }}, '{{ $item->name }}', '{{ $item->price }}' )">
                            <span class="icon"><i class="fa fa-cart-plus"></i></span>
                            <span>Add to Cart</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>   
            @endforeach
        </div>
    </div>

    <div class="modal" style="padding:50px;">
        <div class="modal-background"></div>
        <div class="modal-card"  style="width:400px;">
            <form action="{{ route('add_to_cart') }}" method="POST">
                <header class="modal-card-head">
                    <p class="modal-card-title has-text-centered" id="modal-menu-name"></p>
                    <button type="button" class="delete show-cart" aria-label="close"></button>
                </header>
                <section class="modal-card-body">
                    <table class="table is-bordered is-narrow  is-fullwidth menu-table">
                        <tbody>
                            <tr>
                                <td width="40%" class="has-text-right">Quantity</td>
                                <td width="60%" class="has-text-centered">
                                    <input class="input" type="text" min="1" id="quantity" name="quantity" value="1">
                                    <input type="hidden" id="item_id" name="item_id" value="">
                                </td>
                            </tr>
                            <tr>
                                <td width="40%" class="has-text-right">Price</td>
                                <td width="60%" class="has-text-left">
                                    <span class="item-price"></span>
                                    <input type="hidden" id="price" name="price" value="">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="columns">
                        <div class="column">
                            <h3>Total : </h3>
                            <span id="show-total"></span>
                        </div>
                    </div>
                </section>
                <footer class="modal-card-foot">
                    <button type="submit" class="button is-info">Add to Cart</button>
                    <button type="button" class="button show-cart">Cancel</button>
                </footer>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
  $(".show-cart").click(function(){
    $('.modal').toggle();
  });
  $(function(){
    setTimeout(
      function(){
        $('#success-cart').fadeOut(1000);
      }, 3000
    );
  });

  $(function(){
    $("#quantity").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
  });

  function GetMenuId(id, menuname, itemprice)
  {
    $('#modal-menu-name').html(menuname);
    $('#quantity').val(1);
    $('#item_id').val(id);

    $('input#item_id').val(id);
    $('input#price').val(itemprice);
    $('span.item-price').html(itemprice);
    GetTotal(id);
    $("input[name=quantity]").keyup(function(){
        GetTotal(id);
    });
  }

  function GetTotal(itemid)
  {
      var qty= $('#quantity').val();
      var pricevar = 'price'+itemid;
      var price = $('input[name="price"]').val();
      var total = qty * price;
      $('#show-total').html(total.toFixed(2));
  }
</script>
@endsection
