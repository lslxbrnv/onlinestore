@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="{{ route('categories') }}">Categories</a></li>
                    <li class="is-active"><a href="#" aria-current="page">{{ $categorypage }}</a></li>
                </ul>
            </nav>
            </header>
            <div class="card-content">
                @include('layouts.validation-messages')
                <div class="content">
        
                    <form action="{{ route('submit_category') }}" method="post">
                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="category">Category Name</label>
                                </div>
                            </div>
                            <div class="column is-10">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="name" id="name" value="{{ $category }}">
                                            <input type="hidden" name="id" id="id" value="{{ $categoryid }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal"></div>
                            </div>
                            <div class="column is-2">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            {!! csrf_field() !!}
                                            <input type="submit" class="button is-info is-normal" value="{{ $categorypage }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="column is-8">&nbsp;</div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection
