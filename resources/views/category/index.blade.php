@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-bars"></i>&nbsp;Categories</h3>
        </div>
        <div class="column is-3">
          <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="{{ $search }}">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

     <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr> 
                <th width="7%" class="has-text-centered"><a href="{{ route('add_category') }}"><i class="fa fa-plus-circle fa-lg"></i></a></th>
                <th width="93%">Category</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($categories as $category)
            <tr id="category-row-{{ $category->id }}">   
                <td class="has-text-centered">
                    <a onclick="DeleteCategory({{ $category->id }})" id="delete_item{{ $category->id }}" title="Delete Category">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a> &nbsp; 
                    <a href="{{ route('edit_category', $category->id) }}" title="Edit Category">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </a>
                </td>
                <td>{{ $category->name }}</td>
            </tr>
            @empty
            <tr>   
                <td class="has-text-centered" colspan="2">No Category added.</td>
            </tr>
            @endforelse
        </tbody>
    </table>
<script type="text/javascript">
    function DeleteCategory(categoryid, selector)
    {
        if(confirm("Are you sure to delete this?") ==  true){
            $.ajax({
                type : 'GET',
                url : '{{ route("delete_category") }}',
                data : 'category_id='+categoryid,
                cache : false,
                dataType : "script"
            }).done(function(response) {
                $(selector).closest("tr").remove();
                $('#category-row-'+categoryid).remove();
            }).fail(function(jqXHR, textStatus, y) {    
                //alert(textStatus);
            });
        }
    }
</script>
@endsection
