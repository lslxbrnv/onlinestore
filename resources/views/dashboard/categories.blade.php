@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-bars"></i>&nbsp;Categories</h3>
        </div>
        <div class="column is-3">
          <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

     <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr> 
                <th width="5%" class="has-text-centered"><a href="{{ route('addCategory') }}"><i class="fa fa-plus-circle fa-lg"></i></a></th>
                <th width="95%">Category</th>
            </tr>
        </thead>
        <tbody>
            <tr>   
                <td></td>
                <td>Frozen foods</td>
            </tr>
            <tr>   
                <td></td>
                <td>Drinks</td>
            </tr>
            <tr>   
                <td></td>
                <td>Canned Goods</td>
            </tr>
        </tbody>
    </table>

@endsection
