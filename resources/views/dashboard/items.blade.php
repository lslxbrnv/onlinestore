@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-shopping-cart"></i>&nbsp;Items</h3>
        </div>
        <div class="column is-3">
          <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

     <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr> 
                <th width="5%" class="has-text-centered"><a href="{{ route('addItem') }}"><i class="fa fa-plus-circle fa-lg"></i></a></th>
                <th width="40%">Category</th>
                <th width="55%">Items</th>
            </tr>
        </thead>
        <tbody>
            <tr>   
                <td>tessst</td>
                <td>tessst</td>
                <td>tessst</td>
            </tr>
        </tbody>
    </table>


@endsection
