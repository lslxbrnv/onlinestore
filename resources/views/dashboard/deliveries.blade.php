@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-truck"></i>&nbsp;Deliveries</h3>
        </div>
        <div class="column is-3">
          
        </div>
    </div>

     <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <th width="5%" class="has-text-centered"></th>
                <th width="95%">Deliveries</th>
            </tr>
        </thead>
        <tbody>
            <tr>
             
            </tr>
        </tbody>
    </table>

@endsection
