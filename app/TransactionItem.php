<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    public $table = 'transaction_items';
    public $fillable = [ 'transaction_id', 'price', 'item_id', 'quantity' ];
    public $timestamps = false;

    public function item()
    {
        return $this->hasMany('App\Items', 'id', 'item_id');
    }
}
