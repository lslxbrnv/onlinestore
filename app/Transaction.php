<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $table = 'transaction';
    public $fillable = ['cash_received',  'user_id', 'delivery_date'];
    public $timestamps = true;

     protected $dates = [
        'created_at', 'updated_at'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function transaction()
    {
        return $this->belongsTo('App\TransactionMenu');
    }

    public function transaction_reverse()
    {
        return $this->belongsTo('App\TransactionMenu', 'id');
    }

    public function transaction_user_details()
    {
        return $this->hasMany('App\TransacationUserDetails', 'transaction_id', 'id');
    }

     public function getTransactionNumberAttribute()
    {
        return str_pad($this->id, 10, '0', STR_PAD_LEFT);
    }
    
    public function getCurrentStatusAttribute()
    {
        switch($this->status)
        {
            case 1: return 'Pending';
            break;

            case 2:
              return 'Accepted';
            break;

            case 3:
              return 'Invalid';
            break;

        }
    }
}
