<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Response;

use App\Items;
use App\Category;
use App\SoftDeletes;

use \Carbon\Carbon;

class ItemController extends Controller
{
     public function index(Request $request)
    {
      
    	$search = '';
        if($request->has('search')){
            $search = $request->search;
            $items = Items::with('category')->where('name', 'LIKE', "%$search%")->paginate(10);
        }else{
    		$items = Items::with('category')->paginate(10);
        }
        $data = compact('items', 'search');

        return view('items.index', $data);
    }
    
    public function add_item()
    {
        $pagetype = "Add";

    	$categories = Category::get();

        $itemid = 0;

        $item = Items::whereId($itemid)->first();

    	$data = compact('itemid', 'categories', 'item', 'pagetype');

    	return view('items.create', $data);
    }

    public function submit_item(Request $request)
    {
        if($request->item_id == 0)
        {
            $this->validate($request, [
                'name' => 'required|unique:item,name,NULL,id,category_id,'.$request->category_id.',deleted_at,NULL|max:255',
                'category_id' => 'required',
                'price' => 'required|numeric',
                'details' => 'required|max:255',
                'available' => 'required|max:255',
                'file' => 'required|image|mimes:jpeg|max:2048'
            ]);
            $fileName = "fileName".time().'.'.request()->file->getClientOriginalExtension();
            $request->file->storeAs('public', $fileName);
            
            $item = new Items;
            $item->name = $request->name;
            $item->category_id = $request->category_id;
            $item->price = $request->price;
            $item->details = $request->details;
            $item->supplier_id = 1;
            $item->created_at = Carbon::now();
            $item->file = $fileName;
            $item->save();

            $msg = "Item has been Added.";

        }else{
           # dd($request);
            $checkItem = Items::whereId($request->item_id)->first();
            if($checkItem->name != $request->name)
            {
                $this->validate($request, [
                    'name' => 'required|unique:item,name,NULL,id,category_id,'.$request->category_id.',deleted_at,NULL|max:255'
                ]);
            }

            $this->validate($request, [
                'category_id' => 'required',
                'price' => 'required|numeric',
                'details' => 'required|max:255',
                'available' => 'required|max:255'
            ]);

            if ($request->hasFile('image')) {
                $this->validate($request, [
                'file' => 'required|image|mimes:jpeg|max:2048'
                ]);

                $fileName = "fileName".time().'.'.request()->file->getClientOriginalExtension();
                $request->file->storeAs('public', $fileName);

                $item = Items::find($request->item_id);
                $item->file = $fileName;
                $item->save();
            }

            $item = Items::find($request->item_id);
            $item->name = $request->name;
            $item->category_id = $request->category_id;
            $item->price = $request->price;
            $item->details = $request->details;
            $item->updated_at = Carbon::now();
            $item->save();

            $msg = "Item has been Updated.";

        }   
    	

        $data = [
            'success' => 'Item has been added.',
            'id' => $item->id,
            'name' => $request->name
        ];

        return redirect()->route('edit_item', $request->item_id)->with('success', $msg);    
    }

    public function delete_item(Request $request)
    {
        $item = Items::find($request->item_id);
        $item->delete();
    }

    public function edit_item($itemid)
    {
        $pagetype = "Edit";

        $categories = Category::get();

        $item = Items::whereId($itemid)->first();

        $itempage = 'Edit';

        $data = compact('categories', 'item', 'itempag', 'itemid', 'pagetype');

        return view('items.create', $data);
    }

}
