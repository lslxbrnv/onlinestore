<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Response;

use App\Category;
use App\SoftDeletes;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
    	$search = '';
        if($request->has('search')){
            $search = $request->search;
            $categories = Category::where('name', 'LIKE', "%$search%")->paginate(10);
        }else{
    		$categories = Category::paginate(10);
        }
        $data = compact('categories', 'search');

        return view('category.index', $data);
    }

    public function add_category()
    {
        $category = '';

        $categorypage = 'Add';

        $categoryid = '';

        $data = compact('categorypage', 'category', 'categoryid');

    	return view('category.create', $data);
    }

    public function edit_category($categoryid)
    {
        $category = Category::whereId($categoryid)->pluck('name')->first();

        $categorypage = 'Edit';

        $data = compact('categorypage', 'category', 'categoryid');

        return view('category.create', $data);
    }

    public function submit_category(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required|max:255|unique:category,name,NULL,id,deleted_at,NULL'
        ]);

        if($request->id == ''){
             $x = Category::create($request->only('name'));
            $message = "Category has been added";
            $route = 'add_category';

            return redirect()->route('add_category')->with('success', $message);   
        }else{
            $x = Category::whereId($request->id)->update($request->only('name'));
            $message = "Category has been updated";
            $route = 'edit_category';

            return redirect()->route('edit_category', $request->id)->with('success', $message);   
        }
    }

    public function delete_category(Request $request)
    {
        $category = Category::find($request->category_id);
        $category->delete();
    }
}
