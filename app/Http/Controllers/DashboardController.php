<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard.index');
    }

    public function categories()
	{
	    return view('dashboard.categories');
	}

	public function add_category()
	{
	    return view('dashboard.add-category');
	}

    public function items()
    {
        return view('dashboard.items');
    }

    public function add_item()
	{
	    return view('dashboard.add-item');
	}

     public function deliveries()
    {
        return view('dashboard.deliveries');
    }

     public function users()
    {
        return view('dashboard.users');
    }
}
