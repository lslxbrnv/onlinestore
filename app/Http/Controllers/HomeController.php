<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Auth;
use Carbon;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        #$rows = Advisory::with(['user'])->orderBy('created_at', 'DESC')->paginate(10);

        #$data = compact('rows');
        return view('home');
    }

    public function cust()
    {
        $mytime = Carbon\Carbon::now();
        $mytime = $mytime->toDateTimeString();

        $data = compact('mytime');

        return view('cust.index', $data);
    }

    public function login()
    {
        return redirect()->route('home');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
