<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvisoriesStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required', 
            'description' => 'required'
        ];
    }

     public function attributes()
    {
        return [
            'title' => 'Title', 
            'description' => 'Description'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'numeric'    => ':attribute is not a number.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
