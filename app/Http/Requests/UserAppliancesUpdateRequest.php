<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
#use Auth;

class UserAppliancesUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required', 
            'wattage' => 'required', 
            'hours' => 'required|numeric|min:1|max:24', 
            'days' => 'required|numeric|min:1|max:31'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name', 
            'wattage' => 'Wattage', 
            'hours' => 'Hours',
            'days' => 'Days'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'numeric'    => ':attribute is not a number.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
