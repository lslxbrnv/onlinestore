<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required', 
            'email' => 'required|email|unique:users,email', 
            'role' => 'required'
        ];
    }

     public function attributes()
    {
        return [
            'name' => 'Name', 
            'email' => 'Email', 
            'role' => 'Role'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'email'     => ':attribute is not an email.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
