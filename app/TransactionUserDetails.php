<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionUserDetails extends Model
{
    public $table = 'transaction_user_details';
    public $fillable = [ 'address', 'contact_number', 'location', 'transaction_id'];
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];


    public function transaction()
    {
    	return $this->hasMany('App\Transaction', 'transaction_id');
    }

}
