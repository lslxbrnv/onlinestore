<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Items extends Model
{
    use SoftDeletes;
    public $table = 'item';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $fillable = [
    	'name', 'details', 'category_id', 'supplier_id', 'price'
	];

	public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function supplier()
    {
    	return $this->belongsTo('App\Supplier');
    }

    public function transaction_items()
    {
        return $this->belongsTo('App\TransactionItem', 'item_id', 'id');
    }
}
