<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public $table = 'supplier';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'name', 'address', 'contact_no'
	];

	public function items()
    {
    	return $this->hasMany('App\Items');
    }
}
